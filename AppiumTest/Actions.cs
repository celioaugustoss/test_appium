﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppiumTest
{
    /// <summary>
    /// Classe Controle de ações
    /// </summary>
    class Actions
    {

      

        public void WaitLoad(By by)
        {
            if (!IsElementDisplayed(by))
            {
                Load(by);
            }
        }
           
     

        public void Load(By elemento)
        {
            WebDriverWait Load = new WebDriverWait(Global.driver, TimeSpan.FromSeconds(10));
            Load.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(elemento));
        }



        public bool IsElementDisplayed(By by)
        {
            return Global.driver.FindElement(by).Displayed;
        }

       
       


       
     


    }
}

