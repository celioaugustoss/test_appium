﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace AppiumTest
{
    /// <summary>
    /// Classe de parametros para os Caso de testes
    /// </summary>
    public class Acesso : IEnumerable<TestCaseData>
    {
        public string Classificacao { get; set; }
        public string Menu { get; set; }
        public string ResultExperado_1 { get; set; }
        public string ResultExperado_2 { get; set; } 

        public string ResultExperado_3 { get; set; } 
        public string ResultExperado_4 { get; set; }
        public string ResultExperado_5 { get; set; }
       
        public string NomeFilme { get; set; }

        public string SetName { get; set; }
        public string Controle { get; set; }


        public Acesso(string menu, string resultExperado_1, string resultExperado_2, string resultExperado_3, string resultExperado_4, string resultExperado_5, string classificacao, string nomefilme, string controle, string setName)
        {
            Menu = menu;
            ResultExperado_1 = resultExperado_1;
            ResultExperado_2 = resultExperado_2;
            ResultExperado_3 = resultExperado_3;
            ResultExperado_4 = resultExperado_4;
            ResultExperado_5 = resultExperado_5;
            Classificacao = classificacao;
            NomeFilme = nomefilme;
            Controle = controle;
            SetName = setName;
            
        }

       

        /// <summary>
        /// Caso de testes       
        /// </summary>

        public static Acesso Filmes = new Acesso(menu:"Filmes", resultExperado_1:"Filmes mais populares", resultExperado_2:"Próximas Estreias", resultExperado_3: null, resultExperado_4: null, resultExperado_5: null, classificacao: null, nomefilme: null, controle: "AcessarMenu", setName: "Menu Filmes");


        public static Acesso FilmesPopulares = new Acesso(menu: "Filmes", resultExperado_1: "Filmes mais populares", resultExperado_2: "Próximas Estreias", resultExperado_3: "Filmes mais populares", resultExperado_4: "Comédia", resultExperado_5: "Romance", classificacao: "Ver todos Filmes mais populares", nomefilme: "A Missy Errada", controle: "AcessarClassificacao", setName: "Filmes mais populares");
        

        public static Acesso Pesquisar = new Acesso(menu: "Pesquisar a IMDb", resultExperado_1: "Drama", resultExperado_2: "Guerra", resultExperado_3: null, resultExperado_4: null, resultExperado_5: null, classificacao: null, nomefilme: "1917", controle: "Pesquisar", setName: "Pesquisar");

        public IEnumerator<TestCaseData> GetEnumerator()
        {
            return new List<TestCaseData>
            {
                new TestCaseData(Menu, ResultExperado_1, ResultExperado_2, ResultExperado_3, ResultExperado_4, ResultExperado_5, Classificacao, NomeFilme, Controle).SetName(SetName).SetDescription("Acesso" + SetName) //retorno de variaveis
            }.GetEnumerator();
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
