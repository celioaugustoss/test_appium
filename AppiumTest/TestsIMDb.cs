using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Support.UI;

using SeleniumExtras.WaitHelpers;

namespace AppiumTest
{
    public class TestsIMDb
    {
        Processo processo = new Processo();

        //AndroidDriver<AndroidElement> driver;

        [SetUp]
        public void Setup()
        { 
            
            AppiumOptions options = new AppiumOptions();
            //Capabilitys android
            options.AddAdditionalCapability(AndroidMobileCapabilityType.AppPackage, "com.imdb.mobile");
            options.AddAdditionalCapability(MobileCapabilityType.AutomationName, "uiautomator2");
            options.AddAdditionalCapability(MobileCapabilityType.DeviceName, "emulator-5554");
            options.AddAdditionalCapability(MobileCapabilityType.NewCommandTimeout, 3000);
            options.AddAdditionalCapability(MobileCapabilityType.PlatformName, "Android");            
            options.AddAdditionalCapability("skipServerInstalation", true);            
            options.AddAdditionalCapability("skipDeviceInitialization", true);
            options.AddAdditionalCapability("ignoreUnimportantViews", true);        
            options.AddAdditionalCapability(MobileCapabilityType.NoReset, true);
            options.AddAdditionalCapability(MobileCapabilityType.FullReset, false);                 
            options.AddAdditionalCapability(AndroidMobileCapabilityType.AppActivity, ".HomeActivity");
            //options.AddAdditionalCapability(MobileCapabilityType.App, Global.appPath);

            Global.driver = new AndroidDriver<AndroidElement>(new Uri("http://127.0.0.1:4723/wd/hub"), options);
            
        }

        [TearDown]

        public void closeDriver()
        {
            Global.driver.CloseApp();
            
        }
        /// <summary>
        /// Metodo de Teste - Com tres varia��es de teste 
        /// </summary>
        /// <param name="menu"></param>
        /// <param name="result1"></param>
        /// <param name="result2"></param>
        /// <param name="result3"></param>
        /// <param name="result4"></param>
        /// <param name="result5"></param>
        /// <param name="classificacao"></param>
        /// <param name="nomefilme"></param>
        /// <param name="controle"></param>
        [TestCaseSource(typeof(Acesso), "Filmes")] // Caso de teste 1
        [TestCaseSource(typeof(Acesso), "FilmesPopulares")] // Caso de teste 2
        [TestCaseSource(typeof(Acesso), "Pesquisar")] // Caso de teste 3
        public void IMDb(string menu, string result1, string result2, string result3, string result4, string result5, string classificacao, string nomefilme, string controle)
        {
            Global.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
            Actions action = new Actions();
            action.WaitLoad(By.Id("com.imdb.mobile:id/toolbar"));
          

            switch (controle)
            {
                case "AcessarMenu":
                    processo.AcessarIMDb(menu, result1, result2);
                    break;

                case "AcessarClassificacao":
                    processo.AcessarIMDb(menu, result1, result2);
                    processo.ClassificacaoVerTodos(nomefilme, classificacao, result3, result4, result5);
                    break;
                case "Pesquisar":
                    processo.SearchFilme(menu, nomefilme, result1, result2);
                    break;

                default:
                    break;
            }
            
            
            

        }
    }
}