﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppiumTest
{
    class Processo
    {
        Actions action = null;


        public Processo()
        {
            action = new Actions();
        }

        /// <summary>
        /// Metodo de acesso ao menu
        /// </summary>
        /// <param name="menu"></param>
        /// <param name="result1"></param>
        /// <param name="result2"></param>
        public void AcessarIMDb(string menu = null, string result1 =null, string result2 =null)
        {
            
            Filmes filmes = new Filmes();            
            filmes.WaitLoad(By.Id("com.imdb.mobile:id/toolbar"));
            IWebElement element = Global.driver.FindElement(By.XPath("//androidx.appcompat.app.ActionBar.Tab[@content-desc='"+menu+"']/android.widget.TextView"));
            Assert.That(element.Text, Does.Contain(menu).IgnoreCase);
            element.Click();
            filmes.WaitLoad(By.Id("com.imdb.mobile:id/header_and_see_all"));
            IWebElement element1 = Global.driver.FindElement(By.Id("com.imdb.mobile:id/header"));

            //Asserts de validação
            Assert.Multiple(() =>
            {
                Assert.That(Global.driver.FindElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.TextView\").resourceId(\"com.imdb.mobile:id/header\").text(\""+result1+"\")").Text, Does.Contain(result1).IgnoreCase);

                Assert.That(Global.driver.FindElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.TextView\").resourceId(\"com.imdb.mobile:id/header\").text(\"" + result2 + "\")").Text, Does.Contain(result2).IgnoreCase);
            }
                );

            
        }
        /// <summary>
        /// Metodo que acessa item Ver todos
        /// </summary>
        /// <param name="nomeFilme"></param>
        /// <param name="classificacao"></param>
        /// <param name="result1"></param>
        /// <param name="result2"></param>
        /// <param name="result3"></param>
        public void ClassificacaoVerTodos(string nomeFilme, string classificacao, string result1, string result2, string result3)
        {
            IWebElement element = Global.driver.FindElementByAccessibilityId(classificacao);
            element.Click();
            Assert.That(Global.driver.FindElementById("com.imdb.mobile:id/list_subject").Text, Does.Contain(result1));
            Global.driver.FindElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.TextView\").resourceId(\"com.imdb.mobile:id/label\").text(\""+nomeFilme+"\")").Click();
            
            //Asserts de validação
            Assert.Multiple(() =>{
                action.WaitLoad(By.Id("com.imdb.mobile:id/title"));
                Assert.That(Global.driver.FindElementById("com.imdb.mobile:id/title").Text, Is.EqualTo(nomeFilme));
                Assert.That(Global.driver.FindElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.TextView\").resourceId(\"com.imdb.mobile:id/text\").text(\""+result2+"\")").Text, Does.Contain(result2));
                Assert.That(Global.driver.FindElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.TextView\").resourceId(\"com.imdb.mobile:id/text\").text(\""+result3+"\")").Text, Is.EqualTo(result3));
             
                
            });           


        }

        /// <summary>
        /// Metodo de pesquisa 
        /// </summary>
        /// <param name="search"></param>
        /// <param name="nomefilme"></param>
        /// <param name="result1"></param>
        /// <param name="result2"></param>
        public void SearchFilme(string search, string nomefilme, string result1, string result2)
        {
           
            
            Global.driver.FindElementByAccessibilityId(search).Click();
            action.WaitLoad(By.Id("com.imdb.mobile:id/search_query"));
            Global.driver.FindElementById("com.imdb.mobile:id/search_query").SendKeys(nomefilme);
            Global.driver.FindElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.TextView\").resourceId(\"com.imdb.mobile:id/suggestion\").text(\""+ nomefilme + "\")").Click();

            Assert.Multiple(() => {
                action.WaitLoad(By.Id("com.imdb.mobile:id/title"));
                Assert.That(Global.driver.FindElementById("com.imdb.mobile:id/title").Text, Is.EqualTo(nomefilme));
                Assert.That(Global.driver.FindElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.TextView\").resourceId(\"com.imdb.mobile:id/text\").text(\""+ result1 + "\")").Text, Does.Contain(result1));
                Assert.That(Global.driver.FindElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.TextView\").resourceId(\"com.imdb.mobile:id/text\").text(\""+ result2 + "\")").Text, Is.EqualTo(result2));              
            });
        }

    }
}
